﻿using UnityEngine;
using System.Collections;

namespace SolarWars
{
	public class Starport : ABuilding
	{
		public void Awake()
		{
			name_ = "Starport";
		}

		public override void Start()
		{
			base.Start();
			owner.HQ = this;
		}

		public override void OnCapture()
		{
			Application.LoadLevel("start");
		}
	}
}