﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace SolarWars
{

    public class Unit_Missile : AUnit
    {
        protected override int BaseHP
        {
            get
            {
                return 100;
            }
        }

        protected override int BaseSP
        {
            get { return 25; }
        }

        protected override int BaseMove
        {
            get { return 3; }
        }

        protected override int BaseAtk
        {
            get { return 50; }
        }

        protected override int BaseDef
        {
            get { return 0; }
        }

        public override int Distance(Tile tile)
        {
            return 1;
        }

        public override List<Tile> GetAttackRadius()
        {
            List<Tile> t = Game.getMap().GetTiles(gridX, gridY, 4, Distance, true);
            t.Remove(GetMapTile());
            List<Tile> t2 = Game.getMap().GetTiles(gridX, gridY, 2, Distance, true);
            t2.ForEach(n => t.Remove(n));
            return t;
        }

        // Update is called once per frame
        void Update()
        {
            //UpdateMapPosition();
            // hasActed = false;
        }
    }
}