﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SolarWars
{
    class Unit_LaserInf : AUnit
    {
        protected override int BaseHP
        {
            get
            {
                return 100;
            }
        }

        protected override int BaseSP
        {
            get { return 25; }
        }

        protected override int BaseMove
        {
            get { return 4; }
        }

        protected override int BaseAtk
        {
            get { return 33; }
        }

        protected override int BaseDef
        {
            get { return 0; }
        }

        public override int Distance(Tile tile)
        {
            return 1;
        }
    }
}
