﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SolarWars
{
    class Unit_BeamInf : AUnit
    {
         protected override int BaseHP
        {
            get
            {
                return 100;
            }
        }

        protected override int BaseSP
        {
            get { return 25; }
        }

        protected override int BaseMove
        {
            get { return 3; }
        }

        protected override int BaseAtk
        {
            get { return 50; }
        }

        protected override int BaseDef
        {
            get { return 0; }
        }

        public override int Distance(Tile tile)
        {
            return 1;
        }
    }
}
