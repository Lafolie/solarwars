﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SolarWars;
using UnityEngine;

namespace SolarWars
{

    public abstract class AController
    {
		public Tile hoverTile;
        public List<AUnit> units = new List<AUnit>();
		public List<ABuilding> buildings = new List<ABuilding>();
		public int credits;
		public Starport HQ;
		protected bool dirtyPush = true;

        //unit selections
		private AUnit selectedUnit_;
		public AUnit SelectedUnit
		{
			get
			{
				return selectedUnit_;
			}

			set
			{
				if(value == null)
				{
					Game.GetTilePanel(TilePanel.InfoAlign.Selection).Hide();
				}
				else
				{
					Game.GetTilePanel(TilePanel.InfoAlign.Selection).Show(value.GetMapTile());
				}
				selectedUnit_ = value;
			}
		}

        public AUnit SubSelectedUnit { get; set; }

		public void AddUnit(AUnit unit)
		{
			units.Add(unit);
		}

        //phase stack
        private Stack<ATurnPhase> phase = new Stack<ATurnPhase>();
		protected float changeInputDelay;

        public ATurnPhase PeekPhase()
        {
            if (phase.Count > 0)
                return phase.Peek();

            return null;
        }

        public void PushPhase(ATurnPhase turnPhase)
        {
			ATurnPhase p = PeekPhase();
			if (p != null)
				p.Leave();

            phase.Push(turnPhase);
			dirtyPush = true;
			changeInputDelay = 0.1f;
        }

        public ATurnPhase PopPhase()
        {
			ATurnPhase p = PeekPhase();
			if (p != null && phase.Count > 1)
			{
				p.Leave();
				phase.Pop();
				dirtyPush = true;
				changeInputDelay = 0.1f;
				return p;
			}

            return null;
        }

        public void ClearPhaseHistory()
        {
			if(phase.Count > 0)
				phase.Peek().Leave();
            phase.Clear();
            phase.Push(new Phase_UnitSelect());
			changeInputDelay = 0.1f;
        }

        public abstract void Update();
		public abstract void BeginTurn();
		public abstract void EndTurn();

		protected void CollectCredits()
		{
			credits += buildings.Count * 1000;
		}


    }
}