﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace SolarWars
{
	[Serializable]
	class GameInstance
	{
		public GameInstance()
		{

		}

		public int turn;
		public List<Player> players = new List<Player>();
		public IMap map;

		public int currentPlayerIndex { get { return turn % players.Count; } }
        public Player CurrentPlayer { get { return players[currentPlayerIndex]; } }
	}
}
