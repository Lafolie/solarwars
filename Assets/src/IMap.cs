﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SolarWars
{

    public interface IMap
    {
        Tile GetTile(int gridX, int gridY);
        List<Tile> GetTiles();
        List<Tile> GetTiles(int gridX, int gridY, int radius, Func<Tile, int> distance, bool ignoreResidents = false);
    }
}