﻿using UnityEngine;
using System.Collections;
using System;
using System.Linq;
using System.Collections.Generic;

namespace SolarWars
{

    public class Map : MonoBehaviour, IMap
    {

        public GameObject tilePrefab;

        private Tile[][] tiles;

        private bool test;

        // interface stuff

        public Tile GetTile(int x, int y)
        {
            if (x >= 0 && x < tiles[0].Length && y >= 0 && y < tiles.Length)
            {
                return tiles[y][x];
            }

            print("Yo");
            return null;
        }

        private class TileNode
        {
            public Tile tile;
            public TileNode parent;
            public int distance;

            public TileNode(Tile t, TileNode p, int d)
            {
                tile = t;
                parent = p;
                distance = parent != null ? p.distance + d : d;
            }

        }

        public List<Tile> GetTiles()
        {
            return tiles.SelectMany(n => n).ToList();
        }

        public List<Tile> GetTiles(int x, int y, int radius, Func<Tile, int> distance, bool ignoreResidents = false)
        {
            HashSet<Tile> closed = new HashSet<Tile>();
            List<TileNode> open = new List<TileNode>();

            open.Add(new TileNode(GetTile(x, y), null, 0));

            while (open.Count > 0)
            {
                TileNode t = open[0];
                open.RemoveAt(0);

                if (closed.Contains(t.tile))
                    continue;

                closed.Add(t.tile);
                if (t.distance >= radius)
                    continue;
                foreach (Tile n in t.tile.Neighbours)
                {
                    if (n == null || (n.resident != null && !ignoreResidents))
                        continue;
                    TileNode newT = new TileNode(n, t, distance(n));
                    TileNode openT = open.FirstOrDefault(i => i.tile == n);
                    if (openT != null && openT.parent.distance > t.distance)
                        openT.parent = t;
                    else
                        open.Add(newT);
                }

            }

            return closed.ToList();
        }

        // Use this for initialization
        void Awake()
        {

            // TEMPORARY // generate map
            tiles = new Tile[10][];
            for (int y = 0; y < 10; y++)
            {
                tiles[y] = new Tile[10];
                for (int x = 0; x < 10; x++)
                {
                    GameObject tgo = (GameObject)Instantiate(tilePrefab, new Vector3(16 * x, 0, 16 * y), Quaternion.identity);
                    Tile t = tgo.GetComponent<Tile>();
                    t.gridX = x;
                    t.gridY = y;
                    tiles[y][x] = t;
                    t.name = string.Format("DevTile [{0}/{1}]", x, y);
                }
            }

            // find neighbours

            for (int y = 0; y < tiles.Length; y++)
            {
                for (int x = 0; x < tiles[y].Length; x++)
                {
                    Tile t = tiles[y][x];
                    t.north = y > 0 ? tiles[y - 1][x] : null;
                    t.south = y < tiles.Length - 1 ? tiles[y + 1][x] : null;
                    t.east = x > 0 ? tiles[y][x - 1] : null;
                    t.west = x < tiles[y].Length - 1 ? tiles[y][x + 1] : null;
                }
            }

			Game.RegisterMap(this);
        }

		void Start()
		{
			//Game.Camera.FocusTile(tiles[0][0]);
		}

        // Update is called once per frame
        void Update()
        {
            //if (!test)
            //{
            //	//test the thing
            //	Path<Tile> path = FindPath<Tile>(tiles[0][0], tiles[9][9], 100, (n1, n2) => 1, (n, d) => (d.gridX - n.gridX) + (d.gridY - n.gridY));
            //	foreach (Tile t in path)
            //	{
            //		t.Highlight(Color.green);
            //		Tile.highlightedTiles.Add(t);
            //	}
            //	test = true;
            //}
        }

        // a*

		public static int TempEstFunc(Tile n, Tile d)
		{
			return (d.gridX - n.gridX) + (d.gridY - n.gridY);
		}

        static public Path<Node> FindPath<Node>(
        Node start,
        Node destination,
        int maxDistance,
        Func<Node, Node, int> distance,
        Func<Node, Node, int> estimate)
        where Node : IHasNeighbours<Node>
        {
            var closed = new HashSet<Node>();
            var queue = new PriorityQueue<int, Path<Node>>();
            queue.Enqueue(0, new Path<Node>(start));

            while (!queue.IsEmpty)
            {
                var path = queue.Dequeue();
                if (closed.Contains(path.LastStep))
                    continue;
                if (path.LastStep.Equals(destination))
                    return path;
                closed.Add(path.LastStep);
                foreach (Node n in path.LastStep.Neighbours)
                {
                    if (n == null)
                        continue;
                    int d = distance(path.LastStep, n);
                    var newPath = path.AddStep(n, d);
                    if (maxDistance < 0 || newPath.TotalCost <= maxDistance)
                    {
                        queue.Enqueue(newPath.TotalCost + estimate(n, destination), newPath);
                    }
                }
            }
            return null;
        }
    }
}