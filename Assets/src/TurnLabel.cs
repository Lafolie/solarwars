﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace SolarWars
{
    public class TurnLabel : MonoBehaviour
    {

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            GetComponent<Text>().text = string.Format("Turn {0} - {1}", Game.Turn, Game.CurrentPlayer.name);
        }
    }
}