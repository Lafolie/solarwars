﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace SolarWars
{

    public class CommandButton : MonoBehaviour
    {
		public string cmd;
        // Use this for initialization
        void Awake()
        {
			GetComponentInParent<CommandMenu>().commands.Add(cmd, GetComponent<Button>());
        }

        // Update is called once per frame
        void Update()
        {

        }

        public void Activate(string command)
        {
			AController controller = Game.CurrentController;
			AUnit unit = controller.SelectedUnit;
			switch (command)
			{
				case "move":
					controller.PushPhase(new Phase_Move());
					break;
				case "atk":
					controller.PushPhase(new Phase_Attack());
					break;
				case "cap":
					controller.PushPhase(new Phase_Capture());
					break;
				case "wait":
					unit.hasActed = true;
					unit.hasMoved = true;
					Game.CurrentController.ClearPhaseHistory();
					break;
				default:
					break;
			}
        }
    }
}