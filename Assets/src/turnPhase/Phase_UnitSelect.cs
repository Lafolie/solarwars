﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace SolarWars
{
    public class Phase_UnitSelect : ATurnPhase
    {
        public Phase_UnitSelect()
        {
			Debug.Log("unit select phase");
            SelectedUnit = null;
            Game.director.DisableSelectionPanel();
        }

        public override void Enter()
        {
            SelectedUnit = null;
            Game.director.DisableSelectionPanel();
        }

        public override void Leave()
        {
			
        }

		public override void CancelPressed()
		{
			Game.CurrentController.PushPhase(new Phase_TurnMenu());
		}

        public override void TileSelected(Tile tile)
        {
			//check for unit
			if (tile.resident != null)
			{
				if (tile.resident.hasActed || tile.resident.owner != Game.CurrentController)
					return;

				//Game.Camera.FocusTile(tile);
				AController controller = Game.CurrentController;
				controller.SelectedUnit = tile.resident;
				Game.director.EnableSelectionPanel();

				controller.PushPhase(new Phase_CommandMenu());
			}
			else if (tile.building != null && tile.building.owner == Game.CurrentController)
			{
				Game.CurrentController.PushPhase(new Phase_Build());
			}
        }

    }
}