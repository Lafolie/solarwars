﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace SolarWars
{
	public class Phase_Build : ATurnPhase
	{
		private ABuilding building;
		private EventSystem eventSys;

		public Phase_Build()
		{
			allowTileSelection = false;

			building = Game.CurrentController.hoverTile.building; // might need this later
			eventSys = Game.BuyPanel.GetComponentInParent<EventSystem>();
		}

		public override void Enter()
		{
			eventSys.sendNavigationEvents = true;
			eventSys.SetSelectedGameObject(Game.BuyPanel.GetComponentInChildren<Button>().gameObject);
			Game.BuyPanel.Show();
		}

		public override void Leave()
		{
			eventSys.sendNavigationEvents = false;
			Game.BuyPanel.Hide();
		}

		public override void TileSelected(Tile tile)
		{
			
		}
	}
}
