﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace SolarWars
{
    public class Phase_Move : ATurnPhase
    {
        private List<Tile> tiles;
        private int sp;
        private Tile startTile;

        public Phase_Move()
        {
			Tile tile = SelectedUnit.GetMapTile();
            tiles = Game.getMap().GetTiles(tile.gridX, tile.gridY, SelectedUnit.Move, n => 1); //use flat movement cost for now
			tiles.Add(tile);

            sp = SelectedUnit.SP;
            startTile = Game.getMap().GetTile(SelectedUnit.gridX, SelectedUnit.gridY);
            HighlightTiles(tiles, Color.blue);
        }

		public override void Enter()
		{
			Debug.Log("enter move");
			HighlightTiles(tiles, Color.blue);
			SelectedUnit.Teleport(startTile);
			SelectedUnit.hasMoved = false;
			//Game.Camera.FocusTile(startTile);
		}

        public override void Leave()
        {
			Debug.Log("exit move");
            Tile.ResetHighlights();
        }

		//move unit when tile is selected
        public override void TileSelected(Tile tile)
        {
            if (tiles.Contains(tile))
            {
				SelectedUnit.GeneratePathCommands(tile);
				//Game.director.EnqueueCommand(new Cmd_Move(SelectedUnit, SelectedUnit.GetMapTile(), tile));
                //SelectedUnit.Teleport(tile, false);
				SelectedUnit.hasMoved = true;
				//Game.Camera.FocusTile(tile);

				Game.CurrentController.PushPhase(new Phase_CommandMenu());


            }
        }
    }
}