﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SolarWars
{
	public class Phase_Capture : ATurnPhase
	{
		public override void Enter()
		{
			SelectedUnit.GetMapTile().building.Capture(SelectedUnit.HP, SelectedUnit);
			SelectedUnit.hasActed = true;
			Game.CurrentController.ClearPhaseHistory();
		}

		public override void Leave()
		{

		}
	}
}
