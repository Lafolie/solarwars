﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;

namespace SolarWars
{
	public class Phase_TurnMenu : ATurnPhase
	{
		public Phase_TurnMenu()
		{
			allowTileSelection = false;
		}

		public override void Enter()
		{
			Game.TurnPanel.Show();
			EventSystem.current.firstSelectedGameObject = Game.TurnPanel.topElement;
			EventSystem.current.SetSelectedGameObject(Game.TurnPanel.topElement);
			EventSystem.current.sendNavigationEvents = true;
		}

		public override void Leave()
		{
			Game.TurnPanel.Hide();
			EventSystem.current.sendNavigationEvents = false;
		}
	}
}
