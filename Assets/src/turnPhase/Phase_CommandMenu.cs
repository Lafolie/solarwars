﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace SolarWars
{
	public class Phase_CommandMenu : ATurnPhase
	{
		private static CommandMenu menu;
		private static EventSystem eventSys;

		private bool canAtk;
		private bool canCapture;

		public Phase_CommandMenu()
		{
			allowTileSelection = false;

			if (menu == null)
			{
				menu = Game.Menu;
				eventSys = menu.GetComponentInParent<EventSystem>();
			}

		}

		public override void Enter()
		{
			//setup menu
			menu.commands["move"].interactable = !SelectedUnit.hasMoved;
			menu.commands["atk"].interactable = !SelectedUnit.hasActed;

			//check for buildings
			ABuilding building = SelectedUnit.GetMapTile().building;
			menu.commands["cap"].interactable = (building != null && building.ownerId != Game.CurrentPlayer.PlayerNum);

			Debug.Log("enter menu");
			//EventSystem.current.firstSelectedGameObject = menu.TopElement;
			menu.DefaultHighlight();
			menu.Show();
			//EventSystem.current.SetSelectedGameObject(menu.TopElement);
			(Game.CurrentController as PlayerController).SetHoverTile(SelectedUnit.GetMapTile(), false);
			eventSys.sendNavigationEvents = true;
		}

		public override void Leave()
		{
			menu.Hide();
			eventSys.sendNavigationEvents = false;
		}

		public override void TileSelected(Tile tile)
		{
			
		}

	}
}
