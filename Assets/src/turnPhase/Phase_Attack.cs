﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace SolarWars
{
    public class Phase_Attack : ATurnPhase
    {
        private List<Tile> tiles;

        public Phase_Attack()
        {
			//find attack radius
            tiles = SelectedUnit.GetAttackRadius();
        }

		public override void Enter()
		{
			HighlightTiles(tiles, Color.red);
			Game.Camera.FocusTile(SelectedUnit.GetMapTile());
		}

        public override void Leave()
        {
            Tile.ResetHighlights();
        }

        public override void TileSelected(Tile tile)
        {
            if (tile.resident != null && tiles.Contains(tile))
            {
                tile.resident.TakeDamage(SelectedUnit.Atk); //temporary, replace with choreography enqueue
                SelectedUnit.hasActed = true;
				Game.CurrentController.ClearPhaseHistory();
            }
        }
    }
}