﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace SolarWars
{

    public class SelectionPanel : MonoBehaviour
    {
        public bool mainPanel;
        private List<IStatPanel> panels;
        private delegate void GetTargetInfoDelegate();
        private GetTargetInfoDelegate getTargetInfo;

        // Use this for initialization
        void Start()
        {
            panels = GetComponentsInChildren<IStatPanel>().ToList();
            if (mainPanel)
            {
                getTargetInfo = TargetMainSelection;
            }
            else
            {
                getTargetInfo = TargetSubSelection;
            }
        }

        // Update is called once per frame
        void LateUpdate()
        {
            getTargetInfo();

        }

        public void Hide()
        {
            enabled = false;
            GetComponentsInChildren<MonoBehaviour>().ToList().ForEach(n => n.enabled = false);
        }

        public void Show()
        {
            enabled = true;
            GetComponentsInChildren<MonoBehaviour>().ToList().ForEach(n => n.enabled = true);
        }

        private void TargetMainSelection()
        {
            AUnit unit = Game.CurrentController.SelectedUnit;
            if (unit != null)
                panels.ForEach(n => n.UpdateDisplay(unit));
            else
                Hide();
        }

        private void TargetSubSelection()
        {
            AUnit unit = Game.CurrentController.SubSelectedUnit;
            //print(unit.ToString());
            if (unit != null)
                panels.ForEach(n => n.UpdateDisplay(unit));
            else
                Hide();
        }
    }
}