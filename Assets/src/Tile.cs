﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

namespace SolarWars
{
    public class TileEventArgs : EventArgs
    {
        public AUnit resident;

        public TileEventArgs(AUnit resident)
        {
            this.resident = resident;
        }
    }

    //event delegates
    public delegate void TileClick_Handler(object sender, TileEventArgs e);

    public class Tile : MonoBehaviour, IHasNeighbours<Tile>
    {
        public int movementCost = 1;
        public Tile north;
        public Tile south;
        public Tile east;
        public Tile west;
		public ABuilding building;

        private Color highlightColor = Color.grey;
        public static List<Tile> highlightedTiles = new List<Tile>();
        public int gridX;
        public int gridY;
        public AUnit resident;

        //events
        public event TileClick_Handler TileClickedEvent;

        protected virtual void TileClicked(TileEventArgs e)
        {
            if (TileClickedEvent != null)
                TileClickedEvent(this, e);
        }

        public IEnumerable<Tile> Neighbours
        {
            get
            {
                return new Tile[] { north, south, east, west };
            }
        }

	    // Use this for initialization
	    void Start () 
        {
            SetColor(Color.grey);
	    }
	
	    // Update is called once per frame
	    void Update () {
	
	    }

        public void OnMouseDown()
        {
			if (Game.CurrentPlayer.DeviceType == DeviceType.Keyboard)
			{
				TileClicked(new TileEventArgs(null));
			}
                //Game.director.getCurrentPlayer.SelectUnit = resident;
        }

        public void OnMouseEnter()
        {
		    if(Game.CurrentPlayer.DeviceType == DeviceType.Keyboard)
		    {
			    SetColor(Color.white);
			    (Game.CurrentController as PlayerController).SetHoverTile(this, false);
		    }
        }

        public void OnMouseExit()
        {
		    if(Game.CurrentPlayer.DeviceType == DeviceType.Keyboard)
			    SetColor(highlightColor);
        }

        public void Highlight()
        {
            SetColor(highlightColor);
        }

        private void Highlight_(Color color)
        {
            highlightColor = color;
            SetColor(color);
        }

	    public void Highlight(Color color)
	    {
		    Highlight_(color);
		    highlightedTiles.Add(this);
	    }

        public static void ResetHighlights()
        {
            highlightedTiles.ForEach(t => t.Highlight_(Color.grey));
            highlightedTiles.Clear();
        }

        public void SetColor(Color newColor)
        {
            GetComponent<Renderer>().material.color = newColor;
        }

    }
}