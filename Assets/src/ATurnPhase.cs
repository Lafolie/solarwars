﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using SolarWars;

namespace SolarWars
{

    public abstract class ATurnPhase
    {
		public abstract void Enter();
        public abstract void Leave();

        public virtual void TileSelected(Tile tile) { }
        public virtual void TileBeginHover(Tile tile) { }
        public virtual void TileEndHover(Tile tile) { }
		public bool allowTileSelection = true;

		public virtual void CancelPressed()
		{
			Game.CurrentController.PopPhase();
		}

		protected AUnit SelectedUnit
		{
			get
			{
				return Game.CurrentController.SelectedUnit;
			}

			set
			{
				Game.CurrentController.SelectedUnit = value;
			}
		}

        protected void HighlightTiles(List<Tile> tiles, Color color)
        {
            tiles.ForEach(n => n.Highlight(color));
        }
    }
}