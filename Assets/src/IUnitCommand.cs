﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SolarWars
{

    public interface IUnitCommand
    {
        void Execute(Tank unit);
        void Undo(Tank unit);
    }
}