﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace SolarWars
{
	public class CommandMenu : MonoBehaviour
	{
		public GameObject TopElement;
		public Dictionary<string, Button> commands = new Dictionary<string, Button>();

		void Awake()
		{
			Game.RegisterMenu(this);
			Hide();
		}

		// Use this for initialization
		void Start()
		{

		}

		// Update is called once per frame
		void Update()
		{
			//uber hax
			//if (EventSystem.current.currentSelectedGameObject == null)
			//{
			//	EventSystem.current.SetSelectedGameObject(TopElement);
			//}
		}

		public void Hide()
		{
			enabled = false;
			GetComponentsInChildren<MonoBehaviour>().ToList().ForEach(n => n.enabled = false);
		}

		public void Show()
		{
			enabled = true;
			GetComponentsInChildren<MonoBehaviour>().ToList().ForEach(n => n.enabled = true);
		}

		public void SetSelectedCommand(string command)
		{
			EventSystem.current.SetSelectedGameObject(commands[command].gameObject);
		}

		public void DefaultHighlight()
		{
			foreach (string cmd in new string[]{"move", "atk", "cap", "wait"} )
			{
				if (commands[cmd].interactable)
				{
					//EventSystem.current.firstSelectedGameObject = commands[cmd].gameObject;
					EventSystem.current.SetSelectedGameObject(null);
					EventSystem.current.SetSelectedGameObject(commands[cmd].gameObject);
					break;
				}
			}
		}
	}
}