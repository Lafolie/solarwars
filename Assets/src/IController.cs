﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace SolarWars
{

    public interface IController
    {
        void Update();
        void PushPhase(ATurnPhase turnPhase);
        ATurnPhase PopTurnPhase();
        ATurnPhase PeekTurnPhase();
    }
}