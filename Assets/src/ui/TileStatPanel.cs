﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace SolarWars
{
	public class TileStatPanel : MonoBehaviour
	{
		public enum TileStat {Name, Owner, HP};
		public TileStat stat;

		public void UpdateDisplay(Tile tile)
		{
			switch (stat)
			{
				case TileStat.HP:
					GetComponent<Text>().text = tile.building != null ? tile.building.HP.ToString() : "";
					break;
				case TileStat.Name:
					GetComponent<Text>().text = tile.building != null ? tile.building.Name : tile.name;
					break;
				case TileStat.Owner:
					if (!tile.building || tile.building.ownerId < 0)
					{
						GetComponent<Text>().text = "";
						break;
					}
					GetComponent<Text>().text = Game.GetPlayer(tile.building.ownerId).name;
					break;
				default:
					break;
			}
		}
	}
}