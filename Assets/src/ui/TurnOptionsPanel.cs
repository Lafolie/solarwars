﻿using UnityEngine;
using System.Collections;
using System.Linq;

namespace SolarWars
{
	public class TurnOptionsPanel : MonoBehaviour
	{
		public GameObject topElement;
		void Awake()
		{
			Game.RegisterTurnPanel(this);
			Hide();
		}

		// Use this for initialization
		void Start()
		{

		}

		// Update is called once per frame
		void Update()
		{

		}

		public void EndTurn()
		{
			Game.IncTurn();
		}

		public void Show()
		{
			if (enabled)
				return;
			enabled = true;
			GetComponentsInChildren<MonoBehaviour>().ToList().ForEach(n => n.enabled = true);
		}

		public void Hide()
		{
			if (!enabled)
				return;
			enabled = false;
			GetComponentsInChildren<MonoBehaviour>().ToList().ForEach(n => n.enabled = false);
		}
	}
}