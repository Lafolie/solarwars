﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace SolarWars
{
	public class BuyPanel : MonoBehaviour
	{
		public GameObject[] units;

		public void Awake()
		{
			Game.RegisterBuyPanel(this);
			Hide();
		}

		public void Show()
		{
			if (enabled)
				return;
			enabled = true;
			GetComponentsInChildren<MonoBehaviour>().ToList().ForEach(n => n.enabled = true);
		}

		public void Hide()
		{
			if (!enabled)
				return;
			enabled = false;
			GetComponentsInChildren<MonoBehaviour>().ToList().ForEach(n => n.enabled = false);
		}

		public void BuildUnit(int unitIndex)
		{
			if (Game.CurrentController.credits < 1000)
				return;
			Game.CurrentController.credits -= 1000;
			AUnit unit = GameObject.Instantiate(units[unitIndex]).GetComponent<AUnit>();
			unit.ownderId = Game.CurrentPlayerNum;
			unit.gridX = Game.CurrentController.hoverTile.gridX;
			unit.gridY = Game.CurrentController.hoverTile.gridY;
			unit.hasActed = true;

			Game.CurrentController.ClearPhaseHistory();
		}
	}
}
