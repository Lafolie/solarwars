﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace SolarWars
{

    public class PrimaryStatPanel : MonoBehaviour, IStatPanel
    {
        public enum MainStat { HP, SP };
        public MainStat stat;

        public void UpdateDisplay(AUnit unit)
        {
            if (stat == MainStat.HP)
            {

                GetComponent<Text>().text = string.Format("{0}/{1}", unit.HP, unit.MaxHP);
            }
            else
            {
                GetComponent<Text>().text = string.Format("{0}/{1}", unit.SP, unit.MaxSP);

            }
        }
    }
}