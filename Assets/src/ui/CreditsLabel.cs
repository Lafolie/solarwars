﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace SolarWars
{
	public class CreditsLabel : MonoBehaviour
	{

		// Use this for initialization
		void Start()
		{

		}

		// Update is called once per frame
		void Update() 
		{
			GetComponent<Text>().text = Game.CurrentController.credits.ToString() + "CR";
		}
	}
}