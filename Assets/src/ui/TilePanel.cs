﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System.Collections.Generic;

namespace SolarWars
{
	public class TilePanel : MonoBehaviour
	{
		public enum InfoAlign { Selection, Hover };
		public InfoAlign InfoAlignment;
		private Tile target;

		private List<TileStatPanel> panels;
		// Use this for initialization
		void Start() 
		{
			Game.RegisterTilePanel(this);
			panels = GetComponentsInChildren<TileStatPanel>().ToList();
			Hide();
		}
		
		// Update is called once per frame
		void Update()
		{
			panels.ForEach(n => n.UpdateDisplay(target));
		}

		public void Show(Tile tile)
		{
			target = tile;
			if (enabled)
				return;
		 	enabled = true;
			GetComponentsInChildren<MonoBehaviour>().ToList().ForEach(n => n.enabled = true);
		}

		public void Hide()
		{
			print("hide the info");
			if (!enabled)
				return;
			enabled = false;
			GetComponentsInChildren<MonoBehaviour>().ToList().ForEach(n => n.enabled = false);
		}
	}
}