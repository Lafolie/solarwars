﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SolarWars
{

    public abstract class AUnitCommand
    {
		private AUnit unit;
        abstract public bool Execute();
    }
}