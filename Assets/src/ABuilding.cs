﻿using UnityEngine;
using System.Collections;

namespace SolarWars
{
	public abstract class ABuilding : MonoBehaviour
	{
		public AController owner;
		private int health = 100;
		protected string name_ = "New Building";
		public int ownerId;
		public int gridX, gridY;
		public Tile tile;
		public GameObject[] unitsBuiltHere;

		public string Name
		{
			get
			{
				return name_;
			}
		}
		public int HP
		{
			get
			{
				return health;
			}
		}

		public Tile Location
		{
			get
			{
				return tile;
			}
		}

		// Use this for initialization
		public virtual void Start()
		{
			tile = Game.getMap().GetTile(gridX, gridY);
			tile.building = this;
			transform.position = new Vector3(16 * gridX, 8, 16 * gridY);

			if (ownerId < 0)
				return;
			owner = Game.GetController(ownerId);
			owner.buildings.Add(this);
		}

		// Update is called once per frame
		public virtual void Update()
		{

		}
		public virtual void OnCapture(){}

		public void Capture(int power, AUnit unit)
		{
			health -= Mathf.CeilToInt(power * 0.5f);
			if (health <= 0)
			{
				if (owner != null)
					owner.buildings.Remove(this);
				owner = unit.owner;
				ownerId = unit.ownderId;
				health = 100;
				owner.buildings.Add(this);
				OnCapture();
			}
		}

		public void ResetCapture()
		{
			health = 100;
		}

		public override string ToString()
		{
			return name_;
		}
	}
}