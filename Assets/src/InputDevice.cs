﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine.EventSystems;

namespace SolarWars
{

    //device type enum
    public enum DeviceType { Keyboard, Pad1, Pad2 };

    //buttons table (better way to do this?)
    public struct DeviceButtonTable
    {
        public readonly string axisX1;
        public readonly string axisY1;
        public readonly string axisX2;
        public readonly string axisY2;
        public readonly string confirm;
        public readonly string cancel;
        public readonly string start;

        public DeviceButtonTable(string axisX1, string axisY1, string axisX2, string axisY2, string confirm, string cancel, string start)
        {
            this.axisX1 = axisX1;
            this.axisY1 = axisY1;
            this.axisX2 = axisX2;
            this.axisY2 = axisY2;
            this.confirm = confirm;
            this.cancel = cancel;
            this.start = start;
        }

		//public string AxisX1 { get { return axisX1; } }
		//public string AxisY1 { get { return axisY1; } }
		//public string AxisX2 { get { return axisX2; } }
		//public string AxisY2 { get { return axisY2; } }
		//public string Confirm { get { return confirm; } }
		//public string Cancel { get { return cancel; } }
		//public string Start { get { return start; } }

    }

    public class InputDevice
    {
        //device info
        private DeviceType deviceType;
        public DeviceType getDeviceType { get { return deviceType; } }
        private DeviceButtonTable mappings;

        //constant mapping tables
        private static readonly IList<DeviceButtonTable> ButtonMappings = new ReadOnlyCollection<DeviceButtonTable>
            (new[] { 
                new DeviceButtonTable("keyAxisX1", "keyAxisY1", "keyAxisX2", "keyAxisY2", "keyConfirm", "keyCancel", "keyStart"),
                new DeviceButtonTable("p1AxisX1", "p1AxisY1", "p1AxisX2", "p1AxisY2", "p1Confirm", "p1Cancel", "p1Start"),
                new DeviceButtonTable("p2AxisX1", "p2AxisY1", "p2AxisX2", "p2AxisY2", "p2Confirm", "p2Cancel", "p2Start")
            });

        public InputDevice(DeviceType device)
        {
            deviceType = device;
            mappings = ButtonMappings[(int)device];
        }

        //input query methods
        public bool CursorUp() { return Input.GetAxis(mappings.axisY1) > 0 ? true : false; }
        public bool CursorDown() { return Input.GetAxis(mappings.axisY1) < 0 ? true : false; }
        public bool CursorLeft() { return Input.GetAxis(mappings.axisX1) < 0 ? true : false; }
        public bool CursorRight() { return Input.GetAxis(mappings.axisX1) > 0 ? true : false; }
        public bool Confirm() { return Input.GetButtonDown(mappings.confirm); }
        public bool Cancel() { return Input.GetButtonDown(mappings.cancel); }

		//menu bullshit
		public void BindToMenu()
		{
			StandaloneInputModule sim = EventSystem.current.GetComponent<StandaloneInputModule>();
			sim.horizontalAxis = mappings.axisX1;
			sim.verticalAxis = mappings.axisY1;
			sim.submitButton = mappings.confirm;
			sim.cancelButton = mappings.cancel;
		}
    }
}