﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;

namespace SolarWars
{

    public class PlayerController : AController
    {
        private InputDevice input;


        //input axis buffers
        private float cursorUpTime, cursorDownTime, cursorLeftTime, cursorRightTime;

        public PlayerController(InputDevice input)
        {
            this.input = input;
        }

        public override void Update()
        {
			if (PeekPhase() == null)
				return;

			if (dirtyPush)
			{
				PeekPhase().Enter();
				dirtyPush = false;
			}

			if (PeekPhase().allowTileSelection && changeInputDelay <= 0)
			{
				//move cursor up
				if (input.CursorUp() && Time.fixedTime - cursorUpTime > 0.2f)
				{
                    Debug.Log("bitcheds");
					cursorUpTime = Time.fixedTime;
					Tile newTile = hoverTile.south;
					if (newTile != null)
					{
						SetHoverTile(newTile, true);

					}
				}
				else if (!input.CursorUp())
				{
					cursorUpTime = 0;
				}

				//mvoe cursor down
				if (input.CursorDown() && Time.fixedTime - cursorDownTime > 0.2f)
				{
					cursorDownTime = Time.fixedTime;
					Tile newTile = hoverTile.north;
					if (newTile != null)
					{
						SetHoverTile(newTile, true);

					}
				}
				else if (!input.CursorDown())
				{
					cursorDownTime = 0;
				}

				//move cursor left
				if (input.CursorLeft() && Time.fixedTime - cursorLeftTime > 0.2f)
				{
					cursorLeftTime = Time.fixedTime;
					Tile newTile = hoverTile.east;
					if (newTile != null)
					{
						SetHoverTile(newTile, true);
					}
				}
				else if (!input.CursorLeft())
				{
					cursorLeftTime = 0;
				}

				//move cursor right
				if (input.CursorRight() && Time.fixedTime - cursorRightTime > 0.2f)
				{
					Debug.Log("right");
					cursorRightTime = Time.fixedTime;
					Tile newTile = hoverTile.west;
					if (newTile != null)
					{
						SetHoverTile(newTile, true);
					}
				}
				else if (!input.CursorRight())
				{
					cursorRightTime = 0;
				}

				if (input.Confirm())
				{
					TileSelected(hoverTile);
					Debug.Log("pressed!");
				}
			}

			if (input.Cancel())
			{
				PeekPhase().CancelPressed();
			}

			changeInputDelay -= Time.deltaTime;



            if (hoverTile != null)
                hoverTile.SetColor(Color.white);
        }

		public override void BeginTurn()
		{
			CollectCredits();
			input.BindToMenu();
			ClearPhaseHistory();
			units.ForEach(n =>
			{
				n.hasActed = false;
				n.hasMoved = false;
			});
			
			SetHoverTile(HQ.Location, true);
			hoverTile.SetColor(Color.white);
		}

		public override void EndTurn()
		{
			hoverTile.Highlight();
			ClearPhaseHistory();
		}

        public void TileSelected(Tile tile)
        {
            if (PeekPhase() != null && changeInputDelay <= 0)
                PeekPhase().TileSelected(tile);
        }

        public void SetHoverTile(Tile tile, bool focusCamera)
        {
			if(hoverTile)
				hoverTile.Highlight();
            hoverTile = tile;

			//set camera focus
			if(focusCamera)
				Game.Camera.FocusTile(tile);

			//update ui stuff
			Game.GetTilePanel(TilePanel.InfoAlign.Hover).Show(tile);
            if (tile.resident != null)
            {
                SubSelectedUnit = tile.resident;
                Game.director.EnableHoverPanel();
            }
            else
            {
                Game.director.DisableHoverPanel();
                SubSelectedUnit = null;
            }
        }

    }
}