﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace SolarWars
{

    public class Tank : AUnit
    {
        protected override int BaseHP
        {
            get
            {
                return 100;
            }
        }

        protected override int BaseSP
        {
            get { return 25; }
        }

        protected override int BaseMove
        {
            get { return 5; }
        }

        protected override int BaseAtk
        {
            get { return 50; }
        }

        protected override int BaseDef
        {
            get { return 0; }
        }

        public override int Distance(Tile tile)
        {
            return 1;
        }

        // Update is called once per frame
        void Update()
        {
            //UpdateMapPosition();
           // hasActed = false;
        }
    }
}