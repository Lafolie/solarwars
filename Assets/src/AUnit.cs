﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace SolarWars
{

    public abstract class AUnit : MonoBehaviour
    {
        //state stuff
        public int gridX, gridY;
        public bool hasActed;
		public bool hasMoved;
        public int ownderId;
        public AController owner;
        public int cost = 1000;

        //stats
        private int hp, maxhp, sp, maxsp, move, atk, def;
        public int HP { get { return hp; } }
        public int MaxHP { get { return maxhp; } }
        public int SP { get { return sp; } }
        public int MaxSP { get { return maxsp; } }
        public int Move { get { return move; } }
        public int Atk { get { return Mathf.CeilToInt(atk * hp/100); } }
        public int Def { get { return def; } }

        protected abstract int BaseHP { get; }
        protected abstract int BaseSP { get; }
        protected abstract int BaseMove { get; }
        protected abstract int BaseAtk { get; }
        protected abstract int BaseDef { get; }

        //lambdas
        public abstract int Distance(Tile tile);

        public virtual void Awake()
        {
            hp = BaseHP;
            maxhp = BaseHP;
            sp = BaseSP;
            maxsp = BaseSP;
            move = BaseMove;
            atk = BaseAtk;
            def = BaseDef;
        }

        public virtual void Start()
        {
            UpdateMapPosition();

			transform.position = new Vector3(16 * gridX, 0, 16 * gridY);
            owner = Game.GetPlayer(ownderId).Controller;
            owner.AddUnit(this);

			if (ownderId == 1)
			{
				transform.rotation *= Quaternion.Euler(0, 180, 0);
				GetComponentsInChildren<Renderer>().ToList().ForEach(n => n.material = Game.director.p1mat); //CHANGE THIS LATER
			}
        }

        protected void UpdateMapPosition()
        {
            //tell the map where we are
            Game.getMap().GetTile(gridX, gridY).resident = this;
        }

        public Tile GetMapTile()
        {
            return Game.getMap().GetTile(gridX, gridY);
        }

        public void Teleport(Tile tile, bool moveMesh = true)
        {
            Game.getMap().GetTile(gridX, gridY).resident = null;
            gridX = tile.gridX;
            gridY = tile.gridY;
			if (moveMesh)
				transform.position = new Vector3(16 * gridX, 0, 16 * gridY);

            UpdateMapPosition();
        }

		public void SetPosition(Tile tile)
		{
			Game.getMap().GetTile(gridX, gridY).resident = null;
			gridX = tile.gridX;
			gridY = tile.gridY;
			UpdateMapPosition();
		}

        public virtual void TakeDamage(int amount)
        {
            hp -= amount - def;
            if (hp < 1)
            {
				ABuilding building = GetMapTile().building;
				if (building != null && building.ownerId != ownderId)
					building.ResetCapture();
                Destroy(gameObject);
            }

        }

        public virtual List<Tile> GetAttackRadius()
        {
            List<Tile> t = Game.getMap().GetTiles(gridX, gridY, 1, Distance, true);
            t.Remove(GetMapTile());
            return t;
        }

		// Take a Path<Tile> object and break it down into simple movement commands
		public virtual void GeneratePathCommands(Tile destination)
		{
			List<AUnitCommand> queue = new List<AUnitCommand>();
			Path<Tile> path = Map.FindPath(GetMapTile(), destination, move, (n, d) => 1 + (d.resident == null ? 0 : 9999), Map.TempEstFunc);
			int curDirection = 0;
			Tile end = null;
			Tile previous = null;

			// traverse the path, finding turns
			foreach (Tile t in path)
			{
				print(t);
				if (end == null)
				{
					end = t;
					previous = t;
					continue;
				}

				//determine direction
				int x = previous.gridX - t.gridX;
				int y = previous.gridY - t.gridY;
				int direction = curDirection;

				if (x < 0)
					direction = 1; //left
				if (x > 0)
					direction = 2; //right
				if (y > 0)
					direction = 3; //up
				if (y < 0)
					direction = 4; //down

				//if this is the first step it is not possible to turn
				if (curDirection == 0)
				{
					curDirection = direction;
					previous = t;
					continue;
				}
				print("Drection: " + curDirection + " new direction: " + direction);
				//compare results
				if (curDirection != direction)
				{
					queue.Add(new Cmd_Move(this, previous, end));
					queue.Add(new Cmd_Turn(this, previous, end));
					end = previous;
					curDirection = direction;
					print("enqueued!");
				}

				previous = t;
			}

			Game.director.EnqueueCommand(new Cmd_Turn(this, GetMapTile(), end));
			Game.director.EnqueueCommand(new Cmd_Move(this, GetMapTile(), end)); //enqueue final movement (also makes straight lines work!)

			for (int i = queue.Count - 1; i >= 0; i--)
			{
				Game.director.EnqueueCommand(queue[i]);
			}

			Game.director.EnqueueCommand(new Cmd_Turn(this)); // reset facing direction
		}
    }
}