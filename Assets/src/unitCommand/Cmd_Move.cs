﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace SolarWars
{
	class Cmd_Move : AUnitCommand
	{
		private AUnit unit;
		private Tile start;
		private Tile end;

		private float time = 0;
		private float speed = 0.01f; // time it takes to traves one tile (in seconds)

		public Cmd_Move(AUnit unit, Tile start, Tile end)
		{
			this.unit = unit;
			this.start = start;
			this.end = end;
			Debug.Log(start + " :::: " + end);
			Debug.Log(Vector3.Distance(start.transform.position, end.transform.position));
			speed *= Vector3.Distance(start.transform.position, end.transform.position);
		}

		public override bool Execute()
		{
			time += Time.deltaTime / speed;
			time = Mathf.Min(time, 1);
			unit.transform.position = Vector3.Lerp(start.transform.position, end.transform.position, time);
			unit.SetPosition(end);
			//unit.transform.Rotate(new Vector3(0, 10, 0));
			if(time == 1)
				return true;

			return false;
		}
	}
}
