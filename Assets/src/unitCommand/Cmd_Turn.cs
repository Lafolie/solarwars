﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace SolarWars
{
	class Cmd_Turn : AUnitCommand
	{
		private delegate void InitFunc();

		private AUnit unit;
		private Quaternion rot; // desired rotation
		private Quaternion initRot; // initial rotation
		private Vector3 dir; // destination direction
		private float time = 0;
		private InitFunc init; // used for one-time setup

		// turn to face tile
		public Cmd_Turn(AUnit unit, Tile start, Tile end)
		{
			this.unit = unit;
			dir = end.transform.position - start.transform.position;
			init = InitFaceTile;
		}

		// face team direction
		public Cmd_Turn(AUnit unit)
		{
			this.unit = unit;
			init = InitFaceTeam;
		}

		public override bool Execute()
		{
			if (init != null)
			{
				init();
				init = null;
			}

			time += Time.deltaTime * 10f;
			time = Mathf.Min(time, 1);
			unit.transform.rotation = Quaternion.Lerp(initRot, rot, time);
			Debug.Log(unit.transform.rotation.eulerAngles);
			if(time == 1)
				return true;

			return false;
		}

		// delegates for initialisation, required to get up-to-date unit data without muddying AUnit

		private void InitFaceTile()
		{
			initRot = unit.transform.rotation;
			dir.y = unit.transform.position.y;
			rot = Quaternion.LookRotation(dir, Vector3.up);
		}

		private void InitFaceTeam()
		{
			initRot = unit.transform.rotation;
			rot = unit.ownderId == 0 ? Quaternion.Euler(0, 0, 0) : Quaternion.Euler(0, 180, 0);
		}
	}
}