﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SolarWars;
using UnityEngine;

namespace SolarWars
{
	public static class Game
	{
		public static void Reset()
		{
			instance_ = null;
		}
        private static GameInstance instance_;
        private static GameInstance instance
        {
            get
            {
                if (instance_ == null)
                    instance_ = new GameInstance();
                return instance_;
            }
        }
		private static IMap map_;

		private static Director director_;
		public static Director director
		{
			get
			{
				return director_;
			}

			set
			{
				director_ = value;
			}
		}

		// MENU ELEMENTS
		private static CommandMenu actMenu;
		public static CommandMenu Menu
		{
			get
			{
				return actMenu;
			}
		}
		public static void RegisterMenu(CommandMenu menu)
		{
			actMenu = menu;
		}

		private static TilePanel[] tilePanel = new TilePanel[2];
		public static void RegisterTilePanel(TilePanel panel)
		{
			if (panel.InfoAlignment == TilePanel.InfoAlign.Selection)
			{
				tilePanel[0] = panel;
			}
			else
			{
				tilePanel[1] = panel;
			}
		}
		public static TilePanel GetTilePanel(TilePanel.InfoAlign alignment)
		{
			return alignment == TilePanel.InfoAlign.Selection ? tilePanel[0] : tilePanel[1];
		}

		private static TurnOptionsPanel turnPanel;
		public static void RegisterTurnPanel(TurnOptionsPanel panel)
		{
			turnPanel = panel;
		}
		public static TurnOptionsPanel TurnPanel
		{
			get
			{
				return turnPanel;
			}
		}

		private static BuyPanel buyPanel;
		public static void RegisterBuyPanel(BuyPanel panel)
		{
			buyPanel = panel;
		}
		public static BuyPanel BuyPanel
		{
			get
			{
				return buyPanel;
			}
		}

		// CAMERA ELEMENTS
		private static FollowCam followCam;
		public static FollowCam Camera
		{
			get
			{
				return followCam;
			}
		}

		public static void RegisterCamera(FollowCam cam)
		{
			followCam = cam;
		}



		// GAMEPLAY METHODS
		public static int Turn
		{
			get
			{
				return instance.turn;
			}
		}

		public static void IncTurn()
		{
			CurrentController.EndTurn();
			instance.turn++;
			CurrentController.BeginTurn();
		}

		//object retrieval

		public static IMap getMap()
		{
			return instance.map;
		}

		public static void RegisterMap(IMap map)
		{
			instance.map = map;
			map.GetTiles().ToList().ForEach(n => n.TileClickedEvent += ClickSelection); //register tile events

		}

		public static Player GetPlayer(int index)
		{
			return instance.players[index];
		}

		public static int GetNumPlayers()
		{
			return instance.players.Count();
		}

		public static Player CurrentPlayer
		{
			get
			{
				return instance.CurrentPlayer;
			}
		}

		public static int CurrentPlayerNum
		{
			get
			{
				return instance.currentPlayerIndex;
			}
		}

		public static void AddPlayer(Player newPlayer)
		{
			instance.players.Add(newPlayer);
			newPlayer.PlayerNum = instance.players.Count - 1;
		}

		public static AController GetController(int playerNumber)
		{
			return instance.players[playerNumber].Controller;
		}

		public static AController CurrentController
		{
			get
			{
				return instance.CurrentPlayer.Controller;
			}
		}

		//event methods
		private static void ClickSelection(object sender, TileEventArgs e)
		{
			(CurrentController as PlayerController).TileSelected((Tile)sender);
		}
	}
}