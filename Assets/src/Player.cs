﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace SolarWars
{
	public class Player
	{
		private PlayerController controller;
		private InputDevice input;
		private AUnit selectedUnit;
		private int playerNum_;
		public int PlayerNum
		{
			get
			{
				return playerNum_;
			}

			set
			{
				playerNum_ = value;
				name = String.Format("Player {0}", value + 1);
			}
		}

		public string name;

		public Player(InputDevice inputDevice)
		{
			input = inputDevice;
			controller = new PlayerController(input);
		}

		public DeviceType DeviceType
		{
			get
			{
				return input.getDeviceType;
			}
		}

		public PlayerController Controller
		{
			get
			{
				return controller;
			}
		}

		public void Update()
		{

			if (input.CursorDown())
				Debug.Log("down");

			if (input.CursorUp())
				Debug.Log("up");

			if (input.CursorLeft())
				Debug.Log("left");

			if (input.CursorRight())
				Debug.Log("right");

			controller.Update();
		}

		public AUnit SelectUnit
		{
			set
			{
				selectedUnit = value;
				IEnumerable<Tile> tiles =  Game.getMap().GetTiles(selectedUnit.gridX, selectedUnit.gridY, 3, n => 1);
				Tile.ResetHighlights();
				tiles.ToList().ForEach(t => t.Highlight(Color.blue));
				Tile.highlightedTiles.AddRange(tiles);
			}
		}

		public void DeselectUnit()
		{
			selectedUnit = null;
		}
	}
}