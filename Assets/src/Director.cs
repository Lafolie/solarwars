﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.EventSystems;

// responsible for issuing commands to units in a timely fashion and updating various objects (such as player controllers)
// also marries tile click events with player controllers
namespace SolarWars
{
	public class Director : MonoBehaviour
	{
		//TEMP
		public Material p1mat;

		private List<AUnitCommand> commands = new List<AUnitCommand>(); //super useful for AI


		//ui stuff
		private SelectionPanel leftSelectionPanel;
		private SelectionPanel rightSelectionPanel;

		//temp ui stuff
		public void EnableSelectionPanel()
		{
			leftSelectionPanel.Show();
		}

		public void DisableSelectionPanel()
		{
			leftSelectionPanel.Hide();
		}

		public void EnableHoverPanel()
		{
			rightSelectionPanel.Show();
			//Debug.Log("show me");
		}

		public void DisableHoverPanel()
		{
			rightSelectionPanel.Hide();
		}

		//turn stuff
        //private void IncTurn()
        //{
        //    turn++;
        //    Game.urrentPlayerIndex = currentPlayerIndex == 0 ? 1 : 0;
        //    Game.CurrentPlayer = Game.GetPlayer(currentPlayerIndex);
        //    Game.CurrentController = currentPlayer.Controller;
        //    Game.CurrentController.units.ForEach(n => n.hasActed = false);
        //}

		// Use this for initialization
		void Start()
		{
			//get UI references
			List<SelectionPanel> panels = GetComponentsInChildren<SelectionPanel>().ToList();
			leftSelectionPanel = panels.First(n => n.mainPanel);
			rightSelectionPanel = panels.First(n => !n.mainPanel);

			leftSelectionPanel.Hide();
			rightSelectionPanel.Hide();

			//check whether dev scene was started from the editor (saves going to main menu)
			if (Game.GetNumPlayers() == 0)
			{
				Application.LoadLevel("start");
				return;
			}


			//start the game
			Game.CurrentController.BeginTurn();
		}

		void Awake()
		{
			//register self with game
			Game.director = this;
		}

		// Update is called once per frame
		void Update()
		{
			//print(Game.CurrentPlayerNum);
			//print(Game.CurrentController.units[0].hasActed);
			//if (Game.CurrentController.units.TrueForAll(n => n.hasActed))
			//{
			//	Game.IncTurn();
			//}

			if (!(Game.CurrentController is PlayerController) || commands.Count == 0)
				Game.CurrentController.Update();

			if (commands.Count > 0 && commands[0].Execute())
			{
				commands.RemoveAt(0);
			}
		}

		public void EnqueueCommand(AUnitCommand command)
		{
			commands.Add(command);
		}
	}
}