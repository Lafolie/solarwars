﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SolarWars
{

    public interface ITurnPhase
    {
        void Update(AController controller);
        void Execute();
        void Undo();
    }
}