﻿using UnityEngine;
using System.Collections;

namespace SolarWars
{
	public class FollowCam : MonoBehaviour
	{
		private Vector3 target;
		private Vector3 offset = new Vector3(0, 80, -60);

		void Awake()
		{
			Game.RegisterCamera(this);
		}

		// Use this for initialization
		void Start()
		{

		}

		// Update is called once per frame
		void Update()
		{
			if (offset != target)
			{
				transform.position = Vector3.Lerp(transform.position, target, 0.25f);
				if (Vector3.Distance(transform.position, target) < 0.01)
				{
					transform.position = target;
				}
			}
		}

		public void FocusTile(Tile tile)
		{
			target = tile.transform.position + offset;
		}
	}
}